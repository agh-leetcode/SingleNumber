import java.util.Arrays;

public class SingleOneFinder {
    public static void main(String[] args) {
        SingleOneFinder singleOneFinder = new SingleOneFinder();
        singleOneFinder.runTheApp();
    }

    private void runTheApp() {
        int[] arr = new int[]{4,1,2,1,2};
        System.out.println(singleNumber2(arr));
    }

    private int singleNumber(int[] arr) {
        Set<Integer> set = new HashSet<Integer>();
        for(int i: arr){
            if(!set.add(i)){
                s.remove(i);
            }
        }
        return set.iterator().next();
    }

    //solution using XOR operator
    private int singleNumber2(int[] arr) {
        int result = 0;
        for(int num : arr) {
            result ^= num;
        }

        return result;
    }
}
